import pandas as pd

# Read the original spreadsheet
df = pd.read_excel("employees.xlsx")

# Sort the DataFrame by years of experience in descending order
df_sorted = df.sort_values(by="Years of Experience", ascending=False)

# Create a new DataFrame with only the required columns
df_new = df_sorted[["Name", "Years of Experience"]]

# Write the sorted DataFrame to a new spreadsheet file
df_new.to_excel("employees_sorted.xlsx", index=False)

print("Sorting and saving completed successfully.")
