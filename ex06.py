import random


def guessing_game():
    # Generate a random number between 1 and 9
    secret_number = random.randint(1, 9)

    # Initialize a flag to track if the user has won
    won = False

    # Run the game until the user guesses the correct number
    while not won:
        # Take user input for the guess
        guess = input("Guess the number (between 1 and 9): ")

        # Check if the input is a valid number
        if not guess.isdigit():
            print("Please enter a valid number.")
            continue

        guess = int(guess)

        # Compare the guess with the secret number
        if guess < secret_number:
            print("Too low!")
        elif guess > secret_number:
            print("Too high!")
        else:
            print("YOU WON!")
            won = True

    print("Thanks for playing!")


if __name__ == "__main__":
    guessing_game()
