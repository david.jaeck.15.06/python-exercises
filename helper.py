def youngest_employee(employees):
    youngest = min(employees, key=lambda x: x["age"])
    print("Youngest employee:", youngest["name"], "-", youngest["age"])


def count_upper_lower_case(string):
    upper_count = sum(1 for char in string if char.isupper())
    lower_count = sum(1 for char in string if char.islower())
    print("Number of uppercase letters:", upper_count)
    print("Number of lowercase letters:", lower_count)


def print_even_numbers(numbers):
    even_numbers = [num for num in numbers if num % 2 == 0]
    print("Even numbers:", even_numbers)
