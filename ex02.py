# Define the dictionary
employee = {"name": "Tim", "age": 30, "birthday": "1990-03-10", "job": "DevOps Engineer"}

# Update the job to Software Engineer
employee["job"] = "Software Engineer"

# Remove the age key from the dictionary
del employee["age"]

# Loop through the dictionary and print key:value pairs one by one
for key, value in employee.items():
    print(key + ":", value)

# Define the dictionaries
dict_one = {'a': 100, 'b': 400}
dict_two = {'x': 300, 'y': 200}

# Merge the dictionaries into a new dictionary
merged_dict = {**dict_one, **dict_two}

# Sum up all the values in the new dictionary
total_sum = sum(merged_dict.values())

# Print the sum of all values
print("The merged dictionary:", merged_dict)
print("Sum of all values in the merged dictionary:", total_sum)

# Find the maximum and minimum values
max_value = max(merged_dict.values())
min_value = min(merged_dict.values())

# Print the maximum and minimum values
print("Maximum value in the merged dictionary:", max_value)
print("Minimum value in the merged dictionary:", min_value)
