class Person:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def print_name(self):
        print(f"Full Name: {self.first_name} {self.last_name}")


class Student(Person):
    def __init__(self, first_name, last_name, age):
        super().__init__(first_name, last_name, age)
        self.lectures = []

    def list_lectures(self):
        print(f"{self.first_name}'s lectures:")
        for lecture in self.lectures:
            print("-", lecture)

    def attend_lecture(self, lecture):
        self.lectures.append(lecture)

    def leave_lecture(self, lecture):
        if lecture in self.lectures:
            self.lectures.remove(lecture)
            print(f"{self.first_name} left the lecture: {lecture}")
        else:
            print(f"{self.first_name} is not attending the lecture: {lecture}")


class Professor(Person):
    def __init__(self, first_name, last_name, age):
        super().__init__(first_name, last_name, age)
        self.subjects = []

    def list_subjects(self):
        print(f"{self.first_name}'s subjects:")
        for subject in self.subjects:
            print("-", subject)

    def teach_subject(self, subject):
        self.subjects.append(subject)

    def stop_teaching(self, subject):
        if subject in self.subjects:
            self.subjects.remove(subject)
            print(f"{self.first_name} stopped teaching the subject: {subject}")
        else:
            print(f"{self.first_name} is not teaching the subject: {subject}")


class Lecture:
    def __init__(self, name, max_students, duration):
        self.name = name
        self.max_students = max_students
        self.duration = duration
        self.professors = []

    def print_info(self):
        print(f"Lecture: {self.name}, Duration: {self.duration} hours")

    def add_professor(self, professor):
        self.professors.append(professor)
        print(f"{professor.first_name} is giving the lecture: {self.name}")


# Example usage:
if __name__ == "__main__":
    # Create instances of Student, Professor, and Lecture classes
    student1 = Student("John", "Doe", 20)
    professor1 = Professor("Jane", "Smith", 40)
    lecture1 = Lecture("Introduction to Python", 50, 2)

    # Add lectures and subjects
    student1.attend_lecture("Introduction to Python")
    professor1.teach_subject("Python Programming")
    lecture1.add_professor(professor1)

    # Print information
    student1.print_name()
    student1.list_lectures()
    professor1.print_name()
    professor1.list_subjects()
    lecture1.print_info()
