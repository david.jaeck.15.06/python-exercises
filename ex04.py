import helper

# Example list of dictionaries
employees = [
    {
        "name": "Tina",
        "age": 30,
        "birthday": "1990-03-10",
        "job": "DevOps Engineer",
        "address": {
            "city": "New York",
            "country": "USA"
        }
    },
    {
        "name": "Tim",
        "age": 35,
        "birthday": "1985-02-21",
        "job": "Developer",
        "address": {
            "city": "Sydney",
            "country": "Australia"
        }
    }
]

# Example string
example_string = "Hello World! This is an Example String."

# Example list of numbers
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# Using the functions from the helper module
helper.youngest_employee(employees)
helper.count_upper_lower_case(example_string)
helper.print_even_numbers(numbers)
