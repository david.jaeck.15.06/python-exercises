def calculator():
    num_calculations = 0

    while True:
        # Take user input for operation
        operation = input("Enter operation (plus, minus, multiply, divide) or type 'exit' to quit: ").lower()

        # Check if user wants to exit
        if operation == 'exit':
            print("Exiting calculator.")
            break

        # Increment calculation count
        num_calculations += 1

        # Take user input for two numbers
        try:
            num1 = float(input("Enter first number: "))
            num2 = float(input("Enter second number: "))
        except ValueError:
            print("Error: Only numbers are allowed.")
            continue

        # Perform the operation and print the result
        if operation == 'plus':
            print("Result:", num1 + num2)
        elif operation == 'minus':
            print("Result:", num1 - num2)
        elif operation == 'multiply':
            print("Result:", num1 * num2)
        elif operation == 'divide':
            if num2 == 0:
                print("Error: Division by zero is not allowed.")
            else:
                print("Result:", num1 / num2)
        else:
            print("Invalid operation. Please enter 'plus', 'minus', 'multiply', 'divide', or 'exit'.")

    print("Number of calculations:", num_calculations)


if __name__ == "__main__":
    calculator()
