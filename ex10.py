import requests

def get_public_repositories(username):
    # GitHub API endpoint for getting user's repositories
    url = f"https://api.github.com/users/{username}/repos"

    # Send GET request to the GitHub API
    response = requests.get(url)

    # Check if request was successful
    if response.status_code == 200:
        # Parse JSON response
        repositories = response.json()

        # Print name and URL of each repository
        for repo in repositories:
            print("Repository Name:", repo["name"])
            print("Repository URL:", repo["html_url"])
            print()
    else:
        print("Error:", response.status_code)

if __name__ == "__main__":
    # Input GitHub username
    username = input("Enter GitHub username: ")

    # Call the function to get public repositories
    get_public_repositories(username)
