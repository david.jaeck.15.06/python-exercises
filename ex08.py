from datetime import datetime


def calculate_time_until_birthday(birthday):
    # Get today's date
    today = datetime.today()

    # Parse the birthday string to a datetime object
    try:
        birthday_date = datetime.strptime(birthday, "%Y-%m-%d")
    except ValueError:
        print("Invalid date format. Please enter the date in the format YYYY-MM-DD.")
        return

    # Calculate the time until the next birthday
    next_birthday = datetime(today.year, birthday_date.month, birthday_date.day)
    if today > next_birthday:
        next_birthday = datetime(today.year + 1, birthday_date.month, birthday_date.day)

    time_until_birthday = next_birthday - today

    # Calculate days, hours, and minutes
    days_until_birthday = time_until_birthday.days
    hours_until_birthday, remainder = divmod(time_until_birthday.seconds, 3600)
    minutes_until_birthday, _ = divmod(remainder, 60)

    # Print the result
    print(
        f"Time until your next birthday: {days_until_birthday} days, {hours_until_birthday} hours, and {minutes_until_birthday} minutes.")


if __name__ == "__main__":
    # Take user input for birthday
    birthday = input("Enter your birthday (YYYY-MM-DD): ")

    # Calculate time until the next birthday
    calculate_time_until_birthday(birthday)
